# cygwin-genIso

This Repo is used to port xorriso to cygwin.

A pre-built version is avaliable at the root of this repo.

## Building
1. Install `libiconv-devel` `zlib-devel` and `cygport` in cygwin.
2. clone repo and cd to the `src` dir.
3. open cygwin and navigate to the dir.
4. If only downloaded the .cygport file run `cygport xorriso.cygport download` to download the xorriso src from the gnu website.
5. run `cygport xorriso.cygport all` which will prepare, compile, and package the app.
6. Find the compiled package in `xorriso-1.5.6.pl02-1.x86_64\dist\xorriso\xorriso-1.5.6.pl02-1.tar.xz` this will also create a cygwin src package in the same dir.
7. the packaging proccess may hang (don't know why) so CTRL-C if you see the lines:
```
>>> Creating source package
xorriso-1.5.6.pl02-1.src/
xorriso-1.5.6.pl02-1.src/xorriso-1.5.6.pl02-1.src.patch
xorriso-1.5.6.pl02-1.src/xorriso-1.5.6.pl02.tar.gz
xorriso-1.5.6.pl02-1.src/xorriso.cygport

```
8. install with `tar -C / -xvf xorriso-1.5.6.pl02-1.x86_64/dist/xorriso/xorriso-1.5.6.pl02-1.tar.xz`